# prosemirror-table-sections

Add-on for `prosemirror-tables` to add table and footer rows to tables.

This package consists of three parts that can be used independently:

Schema modifications
====
`addTableSections(tableNodes: TableNodes) : TableNodes` adds an attribute to the spec of table rows to
define the section of the row ('head', 'body' (default), or 'foot'). It can be used like this in combination with `prosemirror-tables`

```
import { tableNodes } from 'prosemirror-tables'
import { addTableSections } from 'prosemirror-tables-sections'

const tableOptions = {
    ... // options for tables
}

const nodes = addTableSections(tableNodes(tableOptions))

```

Commands
====
`toggleRowHeader` and `toggleRowFooter` are commands that toggle the section of the rows of the selection (if any) between 'body' and 'head'/'foot'.


Plugin
====
`tableSections(options = {})` returns a plugin that ensures that the order of sections ('head', 'body', 'foot') within any table are correct with any
change. Available options are `maxOneHead? (boolean)` which ensures that there is not more than one header row and `maxOneFoot? (boolean)` which ensures
that there is not more than one footer row, and `minOneHead? (boolean)`/`minOneFoot? (boolean)` which ensure that there is at least one head/foot row.

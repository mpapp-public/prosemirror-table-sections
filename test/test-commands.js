import {
    EditorState,
    TextSelection,
    NodeSelection
} from "prosemirror-state"
import {
    cellAround,
    CellSelection
} from "prosemirror-tables"
import ist from "ist"

import {
    table,
    headRow,
    bodyRow,
    footRow
} from "./base"
import {
    toggleRowHeader,
    toggleRowFooter
} from "../src/commands"

function resolveCell(doc, tag) {
    if (tag == null) return null
    return cellAround(doc.resolve(tag))
}

function selectionFor(doc) {
    const cursor = doc.tag.cursor
    if (cursor != null) return new TextSelection(doc.resolve(cursor))
    const $anchor = resolveCell(doc, doc.tag.anchor)
    if ($anchor) return new CellSelection($anchor, resolveCell(doc, doc.tag.head) || undefined)
    const node = doc.tag.node
    if (node != null) return new NodeSelection(doc.resolve(node))
}

function test(doc, command, result) {
    let state = EditorState.create({
        doc,
        selection: selectionFor(doc)
    })
    let ran = command(state, tr => state = state.apply(tr))
    if (result == null) ist(ran, false)
    else ist(state.doc, result, (a, b) => a.eq(b))
}


describe.skip("toggleRowHeader", () => {
    it("can turn a body row into a head row", () =>
        test(
            table(bodyRow("<cursor>"), bodyRow(), bodyRow()),
            toggleRowHeader,
            table(headRow(), bodyRow(), bodyRow())
        )
    )

    it("can turn a head row into a body row", () =>
        test(
            table(headRow("<cursor>"), bodyRow(), bodyRow()),
            toggleRowHeader,
            table(bodyRow(), bodyRow(), bodyRow())
        )
    )
})

describe.skip("toggleRowFooter", () => {
    it("can turn a body row into a foot row", () =>
        test(
            table(bodyRow(), bodyRow(), bodyRow("<cursor>")),
            toggleRowFooter,
            table(bodyRow(), bodyRow(), footRow())
        )
    )

    it("can turn a foot row into a body row", () =>
        test(
            table(bodyRow(), bodyRow(), footRow("<cursor>")),
            toggleRowFooter,
            table(bodyRow(), bodyRow(), bodyRow())
        )
    )
})

import {Plugin, PluginKey} from "prosemirror-state"
import {ReplaceStep, ReplaceAroundStep} from "prosemirror-transform"

const key = new PluginKey("tableSections")

const sectionOrder = ['head', 'body', 'foot']

export function fixTable(state, table, pos, tr, options) {
    if (!tr) {
        tr = state.tr
    }
    let section = 'head'
    table.forEach((row, offset, index) => {
        if (options.maxOneHead && index > 0 && row.attrs.section === 'head') {
            if (section === 'head') {
                section = 'body'
            }
            tr.setNodeMarkup(pos + 1 + offset, null, Object.assign({}, row.attrs, {section}))
        } else if (options.maxOneFoot && index < (table.childCount - 1) && row.attrs.section === 'foot') {
            section = 'body'
            tr.setNodeMarkup(pos + 1 + offset, null, Object.assign({}, row.attrs, {section}))
        } else if (options.minOneHead && index === 0 && row.attrs.section !== 'head') {
            // There should at least one head row and it isn't there.
            tr.setNodeMarkup(pos + 1 + offset, null, Object.assign({}, row.attrs, {section: 'head'}))
        } else if (options.minOneFoot && index === (table.childCount - 1) && row.attrs.section !== 'foot') {
            // There should at least one foot row and it isn't there.
            tr.setNodeMarkup(pos + 1 + offset, null, Object.assign({}, row.attrs, {section: 'foot'}))
        } else if (row.attrs.section !== section) {
            if (sectionOrder.indexOf(row.attrs.section) > sectionOrder.indexOf(section)) {
                section = row.attrs.section
            } else {
                tr.setNodeMarkup(pos + 1 + offset, null, Object.assign({}, row.attrs, {section}))
                return
            }
        }
    })
    return tr
}

export function fixTableSections(state, options = {}) {
    let tr
    state.doc.descendants((node, pos) => {
        if (node.type.spec.tableRole === "table") {
            tr = fixTable(state, node, pos, tr, options)
        }
    })
    return tr
}

export function tableSections(options = {}) {
    return new Plugin({
        key,
        appendTransaction(trs, oldState, state) {
            let changedRanges = []
            trs.forEach(tr => {
                tr.steps.forEach(step => {
                    const stepMap = step.getMap()
                    if (step instanceof ReplaceStep || step instanceof ReplaceAroundStep) {
                        stepMap.forEach((from, to) => {
                            changedRanges.push({from, to})
                        })
                    }
                    changedRanges = changedRanges.map(range => ({from: stepMap.map(range.from, -1), to: stepMap.map(range.to, 1)}))
                })
            })
            const affectedTables = [] // Tables in which at least a table cell has been affected.
            changedRanges.forEach(range => {
                state.doc.nodesBetween(range.from, range.to, (node, nodePos) => {
                    if (node.type.name === 'table') { // Note: this adds tables to the list even if just something inside of them has been edited.
                        if (!affectedTables.find(({pos}) => pos === nodePos)) {
                            affectedTables.push({node, pos: nodePos})
                        }
                        return false
                    }
                    return true
                })
            })
            if (affectedTables.length) {
                let tr
                affectedTables.forEach(({node, pos}) => {
                    tr = fixTable(state, node, pos, tr, options)
                })
                if (tr && tr.steps.length) {
                    return tr
                }
            }
            return
        }
    })
}

export function addTableSections(tableNodes) {
    const tableRow = tableNodes.table_row
    tableRow.attrs = {
        section: {
            default: 'body' // 'head', 'body' or 'foot'
        }
    }
    tableRow.parseDOM = [
        {
            tag: 'tr',
            context: 'thead/',
            getAttrs: () => ({section: 'head'})
        },
        {
            tag: 'tr',
            context: 'tfoot/',
            getAttrs: () => ({section: 'foot'})
        },
        {
            tag: "tr",
            getAttrs: dom => ({
              section: dom.dataset.section ? dom.dataset.section : 'body'
            })
        }
    ]
    tableRow.toDOM = function(node) {
        const attrs = {}
        if(node.attrs.section !== 'body') {
            attrs['data-section'] = node.attrs.section
        }
        return ["tr", attrs, 0]
    }
    return tableNodes
}

import buble from "@rollup/plugin-buble"
import nodeResolve from "@rollup/plugin-node-resolve"
import commonJS from "@rollup/plugin-commonjs"
import multiEntry from '@rollup/plugin-multi-entry'
import pkg from './package.json'

const banner = `\
/**
 * ${pkg.name} v${pkg.version}
 * ${pkg.description}
 *
 * @author ${pkg.author}
 * @license ${pkg.license}
 * @preserve
 */
`

export default [{
        input: "src/index.js",
        output: {
            format: "umd",
            file: pkg.main,
            name: "lib",
            sourcemap: true,
            banner
        },
        external: [ 'prosemirror-tables', 'prosemirror-state', 'prosemirror-transform' ],
        plugins: [
            nodeResolve({
                main: true,
                browser: true
            }),
            commonJS({
                include: '../**',
                sourceMap: false
            }),
            buble({
                exclude: "node_modules/**",
                namedFunctionExpressions: false
            })
        ]
    },
    {
        input: "src/index.js",
        output: {
            format: "esm",
            file: pkg.module,
            name: "lib",
            sourcemap: true,
            banner
        },
        external: [ 'prosemirror-tables', 'prosemirror-state', 'prosemirror-transform' ],
        plugins: [
            nodeResolve({
                main: true,
                browser: true
            }),
            commonJS({
                include: '../**',
                exclude: "node_modules/**",
                sourceMap: false
            })
        ]
    },
    // {
    //     input: 'test/test-*.js',
    //     output: {
    //         file: 'dist/test.bundle.js',
    //         name: 'lib',
    //         sourcemap: true,
    //         format: 'iife',
    //         banner,
    //         globals: {
    //             it: 'it',
    //             describe: 'describe'
    //         }
    //     },
    //     external: ['it', 'describe'],
    //     plugins: [
    //         nodeResolve(),
    //         commonJS(),
    //         multiEntry(),
    //         buble()
    //     ]
    // }
]
